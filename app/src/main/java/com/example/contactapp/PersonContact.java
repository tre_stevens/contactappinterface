package com.example.contactapp;

public class PersonContact extends BaseContact {

	private String birthday;
	private String bio;


	// default constructor
	public PersonContact() {

		this.name = "Mike Mitchell";
		this.phoneNumber = "682-985-6985";
		this.email = "Mike@email.com";
		this.address = "1232 My Home Dr";
		this.birthday = "November 3, 2012";
		this.bio = "This is my bio";

	}

	public PersonContact(String name, String phoneNumber, String email, String address, String birthday, String bio) {
		super();
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
		this.birthday = birthday;
		this.bio = bio;

	}

	public String getBirthday() {
		return birthday;
	}

	public String setBirthday(String birthday) {
		return this.birthday = birthday;
	}

	public String getBio() {
		return bio;
	}

	public String setBio(String bio) {
		return this.bio = bio;
	}

	@Override
	public int compareTo(BaseContact o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toString() {
		return "\n" + "Name = " + this.getName() + "\n" + "Phone Number = " + this.getPhoneNumber() + "\n" + "Email = "
				+ this.getEmail() + "\n" + "Address = " + this.getAddress() + "\n" + "Birthday = " + this.getBirthday()
				+ "\n" + "Bio = " + this.getBio();

	}

	@Override
	public void editContact() {
		for (int e = 0; e < bs.list.getTheList().size(); e++) {
			bs.list.getTheList().get(e);
			this.name = setName(name);
			this.phoneNumber = setPhoneNumber(phoneNumber);
			this.email = setEmail(email);
			this.address = setAddress(address);
			this.birthday = setBirthday(birthday);
			this.bio = setBio(bio);

		}
	}

}
