package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditPersonalContact extends AppCompatActivity {

    EditText et_editName, et_editPhone, et_editEmail, et_editAddress, et_editBirthday, et_editBio;
    Button b_saveEdit, b_cancelEdit;
    int positionToEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_personal_contact);

        et_editName = findViewById(R.id.et_editName);
        et_editPhone = findViewById(R.id.et_editPhone);
        et_editEmail = findViewById(R.id.et_editEmail);
        et_editAddress = findViewById(R.id.et_editAddress);
        et_editBirthday = findViewById(R.id.et_editBirthday);
        et_editBio = findViewById(R.id.et_editBio);
        b_saveEdit = findViewById(R.id.b_saveEdit);
        b_cancelEdit = findViewById(R.id.b_cancelEdit);

        // listen for incoming messages
        Bundle incomingMessages = getIntent().getExtras();
        if (incomingMessages != null) {

            // capture the incoming data from the Personal Contact form
            String name = incomingMessages.getString("Name");
            String phone = incomingMessages.getString("Phone");
            String address = incomingMessages.getString("Address");
            String birthday = incomingMessages.getString("Birthday");
            String email = incomingMessages.getString("Email");
            String bio = incomingMessages.getString("Bio");
            positionToEdit = incomingMessages.getInt("edit");

            et_editName.setText(name);
            et_editPhone.setText(phone);
            et_editAddress.setText(address);
            et_editBirthday.setText(birthday);
            et_editEmail.setText(email);
            et_editBio.setText(bio);
        }

        b_saveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get strings from et_view object
                String newName = et_editName.getText().toString();
                String newPhone = et_editPhone.getText().toString();
                String newAddress = et_editAddress.getText().toString();
                String newBirthday = et_editBirthday.getText().toString();
                String newEmail = et_editEmail.getText().toString();
                String newBio = et_editBio.getText().toString();

                Intent i = new Intent(v.getContext(), ContactList.class);

                i.putExtra("edit", positionToEdit);
                i.putExtra("Name", newName);
                i.putExtra("Phone", newPhone);
                i.putExtra("Address", newAddress);
                i.putExtra("Birthday", newBirthday);
                i.putExtra("Email", newEmail);
                i.putExtra("Bio", newBio);

                startActivity(i);
            }
        });

    }
}
