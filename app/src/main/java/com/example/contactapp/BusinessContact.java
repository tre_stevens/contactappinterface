package com.example.contactapp;

public class BusinessContact extends BaseContact {

	private String businessHours;
	private String websiteURL;

	// default constructor
	public BusinessContact() {
		this.name = "Apple";
		this.phoneNumber = "2654896523";
		this.email = "Applesupport@email.com";
		this.address = "123 Apple Rd.";
		this.businessHours = "8am-5pm";
		this.websiteURL = "www.apple.com";

	}

	public BusinessContact(String name, String phoneNumber, String email, String address, String websiteURL,
			String businessHours) {
		super();
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
		this.websiteURL = websiteURL;
		this.businessHours = businessHours;

	}

	public String getBusinessHours() {
		return businessHours;
	}

	public String setBusinessHours(String businessHours) {
		return this.businessHours = businessHours;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public String setWebsiteURL(String websiteURL) {
		return this.websiteURL = websiteURL;
	}

	@Override
	public int compareTo(BaseContact o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toString() {
		return "\n" + "Name = " + this.getName() + "\n" + "Phone Number = " + this.getPhoneNumber() + "\n" + "Email = "
				+ this.getEmail() + "\n" + "Address = " + this.getAddress() + "\n" + "Hours of Operation = "
				+ this.getBusinessHours() + "\n" + "The website = "
				+ this.getWebsiteURL();

	}

	public void openWebPage() {
		for (int web = 0; web < bs.list.getTheList().size(); web++) {
			bs.list.getTheList().get(web);
		}
		System.out.println("here is the page for " + this.name + " ... " + this.getWebsiteURL());
	}

	@Override
	public void editContact() {
		for (int e = 0; e < bs.list.getTheList().size(); e++) {
			bs.list.getTheList().get(e);
			this.name = setName(name);
			this.phoneNumber = setPhoneNumber(phoneNumber);
			this.email = setEmail(email);
			this.address = setAddress(address);
			this.businessHours = setBusinessHours(businessHours);
			this.websiteURL = setWebsiteURL(websiteURL);

		}
	}

}
