package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class createBusinessContact extends AppCompatActivity {

    ImageView iv_icon2;
    EditText et_name2, et_phone2, et_email2, et_address2, et_hours, et_website;
    Button b_savebusinesscontact, b_cancelbusinesscontact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_business_contact);

        iv_icon2 = findViewById(R.id.iv_icon2);
        et_name2 = findViewById(R.id.et_name2);
        et_phone2 = findViewById(R.id.et_phone2);
        et_email2 = findViewById(R.id.et_email2);
        et_address2 = findViewById(R.id.et_address2);
        et_hours = findViewById(R.id.et_hours);
        et_website = findViewById(R.id.et_website);
        b_cancelbusinesscontact = findViewById(R.id.b_cancelbusinesscontact);
        b_savebusinesscontact = findViewById(R.id.b_savebusinesscontact);


        b_savebusinesscontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Get strings from et objects
                String newName2 = et_name2.getText().toString();
                String newPhone2 = et_phone2.getText().toString();
                String newEmail2 = et_email2.getText().toString();
                String newAddress2 = et_address2.getText().toString();
                String newHours = et_hours.getText().toString();
                String newWebsite = et_website.getText().toString();

                Intent j = new Intent(v.getContext(), ContactList.class);

                j.putExtra("Name",newName2);
                j.putExtra("Phone", newPhone2);
                j.putExtra("Email", newEmail2);
                j.putExtra("Address", newAddress2);
                j.putExtra("Hours", newHours);
                j.putExtra("Website", newWebsite);

                // Start ContactList intent
                startActivity(j);
            }
        });

        b_cancelbusinesscontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goHome = new Intent(v.getContext(), MainActivity.class);
                startActivity(goHome);

            }
        });

    }
}
