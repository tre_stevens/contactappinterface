package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    Button b_contacts , b_createContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_contacts = findViewById(R.id.b_contactList);
        b_createContact = findViewById(R.id.b_createContact);

        b_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToContactList = new Intent(v.getContext(), ContactList.class);
                startActivity(goToContactList);

            }
        });

        b_createContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), CreateContact.class);
                startActivity(i);

            }
        });


    }

}
