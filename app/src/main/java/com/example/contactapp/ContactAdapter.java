package com.example.contactapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactAdapter extends BaseAdapter {

    Activity mActivity;
    AddressBook ab;

    public ContactAdapter(Activity mActivity, AddressBook ab) {
        this.mActivity = mActivity;
        this.ab = ab;
    }

    @Override
    public int getCount() {
        return ab.getTheList().size();
    }

    @Override
    public BaseContact getItem(int position) {
        return ab.theList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View oneContactLine;

        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oneContactLine = inflater.inflate(R.layout.contact_oneline, parent ,false);

        TextView tv_name = oneContactLine.findViewById(R.id.tv_name);
        ImageView iv_icon = oneContactLine.findViewById(R.id.iv_icon);

        BaseContact b1 = this.getItem(position);

        int icon_resources_numbers [] = {
                R.drawable.pic1,
                R.drawable.pic2,
                R.drawable.pic3,
                R.drawable.pic4,
                R.drawable.pic5,
                R.drawable.pic6,
                R.drawable.pic7,
                R.drawable.pic8,
                R.drawable.pic9
        };

        tv_name.setText(b1.getName());
        iv_icon.setImageResource(icon_resources_numbers[position]);

        return oneContactLine;
    }
}
