package com.example.contactapp;

public interface DataAccessService {

	 public void writeAllData(BusinessService bs);	 
	 
	 public BusinessService readAllData();

}
