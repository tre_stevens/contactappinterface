package com.example.contactapp;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class AddressBook extends BaseContact {

	public List<BaseContact> theList;

	// default constructor creates an empty list of type BaseContact
	public AddressBook(List<BaseContact> theList) {
		this.theList = new ArrayList<BaseContact>();
	}

	protected AddressBook(){
	    ArrayList <BaseContact>  startingList = new ArrayList<BaseContact>();
		PersonContact p = new PersonContact();
		BaseContact b = new BusinessContact();
	    this.theList = startingList;
        }

	// add one person/business contact to the list
	public <T extends BaseContact> boolean addOne(T contact) {
		// check to see if the contact is in the list before adding it to avoid
		// duplicates
		if (!this.theList.contains(contact)) {
			this.theList.add(contact);
			return true;
		} else {
			return false;
		}

	}

	// remove one person/business contact from the list
	public <E extends BaseContact> boolean removeOne(E contact) {
		// check to see if the contact is in the list before removing it
		if (this.theList.contains(contact)) {
			this.theList.remove(contact);
			return true;
		} else {
			return false;
		}
	}

	// Edit contact information from one person/business
	public <T extends BaseContact> boolean editContact(T contact, T newVersion) {
		// check to see if the contact is in the list before adding it to avoid
		// duplicates
		if (this.theList.contains(contact)) {
			((BaseContact) this.theList).editContact();
			return true;
		} else {
			// delete exiting contact (using input number)
			this.theList.remove(contact);
			// add the replacement
			this.theList.add(newVersion);
			return false;
		}

	}

	public List<BaseContact> getTheList() {
		// TODO Auto-generated method stub
		return theList;
	}

	public List<BaseContact> getPersonList() {
		// start with an empty list
		List<BaseContact> personList = new ArrayList<>();
		// for each item in theList, see if there is a partial string match. If so, add
		// it to the return list
		for (int i = 0; i < theList.size(); i++) {

			BaseContact c = theList.get(i);
			if (c.getClass() == PersonContact.class) {

				personList.add(c);
			}
		}
		return personList;
	}

	public List<BaseContact> getBusinessList() {
		// start with an empty list
		List<BaseContact> businessList = new ArrayList<>();
		// for each item in theList, see if there is a partial string match. If so, add
		// it to the return list
		for (int i = 0; i < theList.size(); i++) {

			BaseContact c = theList.get(i);
			if (c.getClass() == BusinessContact.class) {

				businessList.add(c);
			}
		}
		return businessList;
	}

	public List<BaseContact> searchFor(String searchWord) {

		// start with an empty list
		List<BaseContact> returnList = new ArrayList<>();
		// for each item in theList, see if there is a partial string match. If so, add
		// it to the return list
		for (int i = 0; i < theList.size(); i++) {

			BaseContact c = theList.get(i);
			if (c.name.contains(searchWord)) {

				returnList.add(c);
			}
		}
		return returnList;
	}

    @Override
    public int compareTo(BaseContact o) {
        return 0;
    }
}
