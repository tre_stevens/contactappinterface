package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BusinessProfile extends AppCompatActivity {

    EditText et_editBName, et_editBPhone, et_editBAddress, et_editBEmail, et_editBHoursOfOp, et_editBWebsite;
    Button b_saveBEdit, b_cancelBEdit, b_deleteB, b_textB, b_callB, b_emailB, b_addressB, b_website;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_profile);

        et_editBName = findViewById(R.id.et_editBName);
        et_editBPhone = findViewById(R.id.et_editBPhone);
        et_editBAddress = findViewById(R.id.et_editBAddress);
        et_editBEmail = findViewById(R.id.et_editBEmail);
        et_editBHoursOfOp = findViewById(R.id.et_editBHoursOfOp);
        et_editBWebsite = findViewById(R.id.et_editBWebsite);
        b_saveBEdit = findViewById(R.id.b_saveBEdit);
        b_cancelBEdit = findViewById(R.id.b_cancelBEdit);
        b_textB = findViewById(R.id.b_textB);
        b_callB = findViewById(R.id.b_callB);
        b_emailB = findViewById(R.id.b_emailB);
        b_addressB = findViewById(R.id.b_addressB);
        b_website = findViewById(R.id.b_website);
        b_deleteB = findViewById(R.id.b_deleteB);


        // listen for incoming data
        Bundle incomingIntent = getIntent().getExtras();

        if(incomingIntent != null){
            String name = incomingIntent.getString("Name2");
            String phone = incomingIntent.getString("Phone2");
            String email = incomingIntent.getString("Email2");
            String address = incomingIntent.getString("Address2");
            String hours = incomingIntent.getString("Hours");
            String website = incomingIntent.getString("Website");

            // fill in the form
            et_editBName.setText(name);
            et_editBPhone.setText(phone);
            et_editBEmail.setText(email);
            et_editBAddress.setText(address);
            et_editBHoursOfOp.setText(hours);
            et_editBWebsite.setText(website);

        }

        b_saveBEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get strings from the et objects
                String newName = et_editBName.getText().toString();
                String newPhone = et_editBPhone.getText().toString();
                String newEmail = et_editBEmail.getText().toString();
                String newAddress = et_editBAddress.getText().toString();
                String newHours = et_editBHoursOfOp.getText().toString();
                String newWebsite = et_editBWebsite.getText().toString();

                // Start the Contact List Activity again

                Intent i = new Intent(v.getContext(), ContactList.class);

                i.putExtra("Name", newName);
                i.putExtra("Phone", newPhone);
                i.putExtra("Email", newEmail);
                i.putExtra("Address", newAddress);
                i.putExtra("Hours", newHours);
                i.putExtra("Website", newWebsite);

                // Start the activity
                startActivity(i);

            }
        });

        b_deleteB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = "delete";
                Intent i = new Intent(v.getContext(), ContactList.class);
                i.putExtra("Name", name);
                // Start the activity
                startActivity(i);
            }
        });

    }
}
