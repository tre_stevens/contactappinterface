package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ContactList extends AppCompatActivity {

    TextView tv_home, tv_search;
    ListView lv_people;
    ContactAdapter adapter;
    AddressBook ab;
    Button b_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        tv_home = findViewById(R.id.tv_home3);
        tv_search = findViewById(R.id.tv_search);
        lv_people = findViewById(R.id.lv_people);
        b_create = findViewById(R.id.b_create);

        ab = ((MyApplication) this.getApplication()).getAddressBook();

        adapter = new ContactAdapter(ContactList.this, ab);

        lv_people.setAdapter(adapter);

        // listen for incoming messages
        Bundle incomingMessages = getIntent().getExtras();
        if(incomingMessages != null) {
            // capture the incoming data from the Personal Contact form
            String name = incomingMessages.getString("Name");
            String phone = incomingMessages.getString("Phone");
            String address = incomingMessages.getString("Address");
            String birthday = incomingMessages.getString("Birthday");
            String email = incomingMessages.getString("Email");
            String bio = incomingMessages.getString("Bio");
            String hours = incomingMessages.getString("Hours");
            String website = incomingMessages.getString("Website");

            // create a new person object
            PersonContact p = new PersonContact(name, phone, email, address, birthday, bio);
            BusinessContact b = new BusinessContact(name,phone,email,address,website,hours);
            if (name.equals("delete")){
                ab.getTheList().remove(p);
                ab.getTheList().remove(b);
                adapter.notifyDataSetChanged();
            }
            else if(website.equals("")){
                ab.getTheList().add(p);
                adapter.notifyDataSetChanged();
            } else {
                ab.getTheList().add(b);
                adapter.notifyDataSetChanged();
            }

        }
        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToMainActivity = new Intent(v.getContext(), MainActivity.class);
                startActivity(goToMainActivity);
            }
        });

        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToSearchContacts = new Intent(v.getContext(), SearchContacts.class);
                startActivity(goToSearchContacts);
            }
        });

        b_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToCreateContact = new Intent(v.getContext(), CreateContact.class);
                startActivity(goToCreateContact);
            }
        });

        lv_people.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               if (ab.getTheList().get(position) instanceof PersonContact){
                   editPersonContact(position);
               } else if (ab.getTheList().get(position) instanceof BusinessContact){
                   editBusinessContact(position);
               }
            }
        });
    }

    public void editPersonContact(int position){
        Intent i = new Intent(getApplicationContext(), ContactProfile.class);
        // Get the contents of person at position
        PersonContact p = (PersonContact) ab.getTheList().get(position);
        i.putExtra("Name", p.getName());
        i.putExtra("Phone", p.getPhoneNumber());
        i.putExtra("Email", p.getEmail());
        i.putExtra("Address", p.getAddress());
        i.putExtra("Birthday", p.getBirthday());
        i.putExtra("Bio", p.getBio());
        i.putExtra("edit", position);
        // Remove the initial contact from the list
        ab.getTheList().remove(p);
        // Start the activity
        startActivity(i);
    }

    public void editBusinessContact(int position){
        Intent j = new Intent(getApplicationContext(), BusinessProfile.class);
        // Get the contents of person at position
        BusinessContact b = (BusinessContact) ab.getTheList().get(position);
        j.putExtra("Name2", b.getName());
        j.putExtra("Phone2", b.getPhoneNumber());
        j.putExtra("Email2", b.getEmail());
        j.putExtra("Address2", b.getAddress());
        j.putExtra("Hours", b.getBusinessHours());
        j.putExtra("Website", b.getWebsiteURL());
        j.putExtra("edit", position);
        // Remove the initial contact from the List
        ab.getTheList().remove(b);
        // Start the activity
        startActivity(j);
    }
}
