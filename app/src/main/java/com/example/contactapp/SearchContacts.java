package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SearchContacts extends AppCompatActivity {

    TextView tv_back;
    TextView tv_home2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contacts);

        tv_back = findViewById(R.id.tv_back2);
        tv_home2 = findViewById(R.id.tv_home2);

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToContactList = new Intent(v.getContext(), ContactList.class);
                startActivity(goToContactList);

            }
        });

        tv_home2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goHome = new Intent(v.getContext(), MainActivity.class);
                startActivity(goHome);

            }
        });
    }
}
