package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ContactProfile extends AppCompatActivity {

    EditText et_editPName, et_editPPhone, et_editPAddress, et_editPBirthday, et_editPEmail, et_editPBio;
    Button b_savePEdit, b_deleteP, b_cancelPEdit, b_callP, b_textP, b_emailP, b_addressP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalcontact_profile);

        et_editPName = findViewById(R.id.et_editPName);
        et_editPPhone = findViewById(R.id.et_editPPhone);
        et_editPAddress = findViewById(R.id.et_editPAddress);
        et_editPBirthday = findViewById(R.id.et_editPBirthday);
        et_editPEmail = findViewById(R.id.et_editPEmail);
        et_editPBio = findViewById(R.id.et_editPBio);
        b_savePEdit = findViewById(R.id.b_savePEdit);
        b_cancelPEdit = findViewById(R.id.b_cancelPEdit);
        b_callP = findViewById(R.id.b_callP);
        b_textP = findViewById(R.id.b_textP);
        b_emailP = findViewById(R.id.b_emailP);
        b_addressP = findViewById(R.id.b_addressP);
        b_deleteP = findViewById(R.id.b_deleteP);


        // listen for incoming data
        Bundle incomingIntent = getIntent().getExtras();

        if(incomingIntent != null){
            String name = incomingIntent.getString("Name");
            String phone = incomingIntent.getString("Phone");
            String address = incomingIntent.getString("Address");
            String birthday = incomingIntent.getString("Birthday");
            String email = incomingIntent.getString("Email");
            String bio = incomingIntent.getString("Bio");

            //fill in the form
            et_editPName.setText(name);
            et_editPPhone.setText(phone);
            et_editPAddress.setText(address);
            et_editPBirthday.setText(birthday);
            et_editPEmail.setText(email);
            et_editPBio.setText(bio);

        }
        b_savePEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newName = et_editPName.getText().toString();
                String newPhone = et_editPPhone.getText().toString();
                String newAddress = et_editPAddress.getText().toString();
                String newBirthday = et_editPBirthday.getText().toString();
                String newEmail = et_editPEmail.getText().toString();
                String newBio = et_editPBio.getText().toString();
                String website = "";

                // Pass this information back to the Contact Class Activity

                Intent j = new Intent(v.getContext(), ContactList.class);

                j.putExtra("Name", newName);
                j.putExtra("Phone", newPhone);
                j.putExtra("Address", newAddress);
                j.putExtra("Birthday", newBirthday);
                j.putExtra("Email", newEmail);
                j.putExtra("Bio", newBio);
                j.putExtra("Website", website);

                // Start the activity
                startActivity(j);

            }
        });

        b_deleteP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = "delete";
                Intent i = new Intent(v.getContext(), ContactList.class);
                i.putExtra("Name", name);
                // Start the activity
                startActivity(i);
            }
        });

    }




}
