package com.example.contactapp;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class BaseContact implements Comparable<BaseContact> {

	// call the BusinessService
	public BusinessService bs;

	public String name;
	public String phoneNumber;
	public String email;
	public String address;
	
	// Super constructor
	public BaseContact(String name, String phoneNumber, String address, String email) {
		super();
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
        bs = new BusinessService();
    }

	// Default constructor
	public BaseContact() {

    }

	public String getName() {
		return name;
	}

	public String setName(String name) {
		return this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String setPhoneNumber(String phoneNumber) {
		return this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public String setAddress(String address) {
		return this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public String setEmail(String email) {
		return this.email = email;
	}

	@Override
	public String toString() {
		return "Name = " + this.getName() + " Phone Number = " + this.getPhoneNumber() + " Email = " + this.getEmail()
				+ " Address = " + this.getAddress();

	}

	public void callContact() {
		for (int call = 0; call < bs.list.getTheList().size(); call++) {
			bs.list.getTheList().get(call);
		}
		System.out.println("Calling... " + this.getName());

	}

	public void messageContact() {
		for (int text = 0; text < bs.list.getTheList().size(); text++) {
			bs.list.getTheList().get(text);
		}
		System.out.println("Messaging... " + this.getName());
	}

	public void emailContact() {
		for (int email = 0; email < bs.list.getTheList().size(); email++) {
			bs.list.getTheList().get(email);
		}
		System.out.println("Emailing... " + this.getName());

	}

	public void navigateTo() {
		for (int directions = 0; directions < bs.list.getTheList().size(); directions++) {
			bs.list.getTheList().get(directions);
		}
		System.out.println("Getting directions to... " + this.getAddress() + " for the contact: " + this.getName());
	}

	/*
	 * public void openWebPage() { for (int web = 0; web <
	 * bs.list.getBusinessList().size(); web++) {
	 * bs.list.getBusinessList().get(web); }
	 * System.out.println("here is the page for " + this.name + " ... " +
	 * this.getWebsiteURL()); }
	 */

	public void editContact() {
		for (int e = 0; e < bs.list.getTheList().size(); e++) {
			bs.list.getTheList().get(e);
			this.name = setName(name);
			this.phoneNumber = setPhoneNumber(phoneNumber);
			this.email = setEmail(email);
			this.address = setAddress(address);

		}
	}

	@Override
	public int compareTo(BaseContact o) {
		return 0;
	}
}
