package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CreateContact extends AppCompatActivity {

    TextView tv_home3;
    Button b_personalContact , b_businessContact;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        tv_home3 = findViewById(R.id.tv_home3);
        b_personalContact = findViewById(R.id.b_personalContact);
        b_businessContact = findViewById(R.id.b_businessContact);

        tv_home3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goHome = new Intent(v.getContext(), MainActivity.class);
                startActivity(goHome);
            }
        });


        b_personalContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent createPersonalContact = new Intent(v.getContext(), CreatePersonalContact.class);
                startActivity(createPersonalContact);

            }
        });

        b_businessContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent createBusinessContact = new Intent(v.getContext(), com.example.contactapp.createBusinessContact.class);
                startActivity(createBusinessContact);

            }
        });

    }
}