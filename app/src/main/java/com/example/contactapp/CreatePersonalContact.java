package com.example.contactapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


public class CreatePersonalContact extends AppCompatActivity {

    ImageView iv_icon;
    EditText et_name, et_phone, et_email, et_address, et_birthday, et_bio;
    Button b_save, b_cancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_personal_contact);

        iv_icon= findViewById(R.id.iv_icon);
        et_name = findViewById(R.id.et_name);
        et_phone = findViewById(R.id.et_phone);
        et_email = findViewById(R.id.et_editEmail);
        et_address = findViewById(R.id.et_address);
        et_birthday = findViewById(R.id.et_birthday);
        et_bio = findViewById(R.id.et_bio);
        b_save = findViewById(R.id.b_save);
        b_cancel= findViewById(R.id.b_cancel);

        b_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get string from et objects
                String newName = et_name.getText().toString();
                String newPhone = et_phone.getText().toString();
                String newAddress = et_address.getText().toString();
                String newBirthday = et_birthday.getText().toString();
                String newEmail = et_email.getText().toString();
                String newBio = et_bio.getText().toString();
                String website = "";

                Intent i = new Intent(v.getContext(), ContactList.class);

                i.putExtra("Name", newName);
                i.putExtra("Phone",newPhone);
                i.putExtra("Address",newAddress);
                i.putExtra("Birthday", newBirthday);
                i.putExtra("Email", newEmail);
                i.putExtra("Bio", newBio);
                i.putExtra("Website", website);
                // start contactList intent
                startActivity(i);

            }
        });

    }
}
