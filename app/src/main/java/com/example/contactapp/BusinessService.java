package com.example.contactapp;

public class BusinessService {

	public AddressBook list;

	public BusinessService(AddressBook list) {
		super();
		this.setList(list);
	}

	protected BusinessService() {
		super();
		this.setList(new AddressBook());
	}

	public void writeList() {
		DataAccessService das =  new FileIOService();
		das.writeAllData(this);
	}

	public AddressBook getList() {
		return list;
	}

	public void setList(AddressBook list) {
		this.list = list;
	}
	
	public void saveList() {
		DataAccessService das =  new FileIOService();
		this.list = das.readAllData().getList();
	}
	

}
